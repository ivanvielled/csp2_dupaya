<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function rent()
    {
    	return $this->belongsTo('App\Club', 'club_id');
    }

    public function status()
    {
    	return $this->belongsTo('App\Status', 'status_id');
    }
}
