<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\User;
use App\Club;
use App\Category;
use App\Brand;
use App\Color;
use App\Availability;
use App\Status;
use Auth;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->user_role == 'admin') {
            $users = User::all();
            $transactions = Transaction::all();

            return view('transactions', [
                'users' => $users,
                'transactions' => $transactions
            ]);
        } else {
            $userId = Auth::user()->id;
            $transactions = Transaction::where('user_id', $userId)->get();

            return view('transactions', [
                'transactions' => $transactions,
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rent = new Transaction;

        $rent->user_id = $request->user_id;
        $rent->club_id = $request->club_id;

        $rent->status_id = 4;
        $rent->save();

        $request->session()->flash('message', 'Your request has been sent for approval.');

        return redirect('/menu');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function rent($id)
    {
        $clubs = Club::find($id);
        $categoryOptions = Category::all();
        $brandOptions = Brand::all();
        $colorOptions = Color::all();
        $availabilityOptions = Availability::all();
        return view('rent', [
            'clubs' => $clubs,
            'categories' => $categoryOptions, 
            'brands' => $brandOptions, 
            'colors' => $colorOptions, 
            'availabilities' => $availabilityOptions
        ]);
    }

    public function proceed()
    {
        $clubs = Club::all();
        $users = User::all();
        $transactions = Transaction::all();

        return view('rent', [
            'clubs' => $clubs,
            'users' => $users,
            'transactions' => $transactions
        ]);
    }

    public function accept($id)
    {
        $transactions = Transaction::find($id);
        $user = User::find($transactions->user_id);
        $club = Club::find($transactions->club_id);

        $transactions->status_id = 3;
        $transactions->save();

        $club->availability_id = 2;
        $club->save();

        $user->has_rented = 1;
        $user->save();

        return redirect('transactions');
    }
}
