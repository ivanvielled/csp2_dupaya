<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Club;
use App\Category;
use App\Brand;
use App\Color;
use App\Availability;
use App\Transaction;
use App\Status;

class ClubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clubs = Club::all();
        $categories = Category::all();
        $brands = Brand::all();
        $colors = Color::all();
        $availabilities = Availability::all();
        $transactions = Transaction::all();
        return view('clubs.index', [
            'clubs' => $clubs,
            'categories' => $categories,
            'brands' => $brands,
            'colors' => $colors,
            'availabilities' => $availabilities,
            'transactions' => $transactions
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoryOptions = Category::all();
        $brandOptions = Brand::all();
        $colorOptions = Color::all();
        $availabilityOptions = Availability::all();
        return view('clubs.create', [
            'categories' => $categoryOptions, 'brands' => $brandOptions, 'colors' => $colorOptions, 'availabilities' => $availabilityOptions
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $club = new Club;

        $club->name = $request->name;
        $club->description = $request->description;
        $club->category_id = $request->category_id;
        $club->brand_id = $request->brand_id;
        $club->color_id = $request->color_id;
        $club->availability_id = $request->availability_id;

        $path = $request->image->store('images', 'public');
        $club->image_location = $path;

        $club->save();
        $request->session()->flash('message', 'The item has been added.');

        return redirect('/menu');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clubs = Club::find($id);
        $categories = Category::all();
        $brands = Brand::all();
        $colors = Color::all();
        $availabilities = Availability::all();

        return view('clubs.edit', [
            'clubs' => $clubs,
            'categories' => $categories,
            'brands' => $brands,
            'colors' => $colors,
            'availabilities' => $availabilities
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $club = Club::find($id);

        $club->name = $request->name;
        $club->description = $request->description;
        $club->category_id = $request->category_id;
        $club->brand_id = $request->brand_id;
        $club->color_id = $request->color_id;
        $club->availability_id = $request->availability_id;

        if ($request->hasFile('image')) {
            // remove the old file.
            Storage::disk('public')->delete($club->image_location);
            // save the new image
            $path = $request->image->store('images', 'public');
            $club->image_location = $path;
        }

        $club->save();
        $request->session()->flash('message', 'The item has been updated');

        return redirect('/menu');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $club = Club::find($id);

        $club->is_archived = 1;
        $club->save();

        $club->delete();

        $request->session()->flash('message', 'The item has been deleted.');

        return redirect('/menu');
    }

    public function deleteConfirm($id)
    {
        $club = Club::find($id);
        $category = Category::find($club->category_id);
        $brand = Brand::find($club->brand_id);
        $color = Color::find($club->color_id);
        $availability = Availability::find($club->availability_id);

        return view('clubs.delete', [
            'clubs' => $club,
            'categories' => $category,
            'brands' => $brand,
            'colors' => $color,
            'availabilities' => $availability
        ]);
    }
}
