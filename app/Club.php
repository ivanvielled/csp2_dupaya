<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    public $timestamps = false;

    public function category()
    {
    	return $this->belongsTo('App\Category', 'category_id');
    }

    public function brand()
    {
    	return $this->belongsTo('App\Brand', 'brand_id');
    }

    public function color()
    {
    	return $this->belongsTo('App\Color', 'color_id');
    }
    
    public function availability()
    {
        return $this->belongsTo('App\Availability', 'availability_id');
    }

    public function transaction()
    {
        return $this->belongsTo('App\Transaction');
    }
}
