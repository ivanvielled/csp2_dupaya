<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    public $timestamps = false;

    public function brands()
    {
    	return $this->hasMany('App\Club', 'brand_id');
    }
}
