<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Availability extends Model
{
    public $timestamps = false;

    public function availabilities()
    {
    	return $this->hasMany('App\Club', 'availability_id');
    }
}
