@if (Auth::user()->user_role != 'admin')
    <script>window.location = '/welcome'</script>
@endif

@extends('layouts.app')

@section('title', 'View Users')

@section('users')

	@foreach ($users as $user)
	
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Name</th>
					<th>Username</th>
					<th>Email</th>
					<th></th>
				</tr>
			</thead>

			<tbody>
				<tr>
					<td>{{ $user->name }}</td>
					<td>{{ $user->username }}</td>
					<td>{{ $user->email }}</td>
					<td>
						<div class="btn-group btn-block">
							<a class="btn btn-outline-info" href='{{ url("/users/$user->id/edit") }}'>Edit</a>
							<a class="btn btn-outline-danger" href='{{ url("/users/$user->id/delete-confirm") }}'>Delete</a>
						</div>
					</td>
				</tr>
			</tbody>
		</table>

	@endforeach

@endsection

@section('content')
	<div class="container-fluid">
		<h3>Transactions</h3>
		<div>
			@yield('users')
		</div>
	</div>
@endsection