@extends('layouts.app')

@section('title', 'Rent Item')

@section('rent-item-form')

	<form action='{{ url("/rent/store") }}' method="post" enctype="multipart/form-data">

		@csrf

			<div class="form-group">
				<label>Borrower: {{ Auth::user()->username }}</label>
				<input type="hidden" class="form-control" name="user_id" value="{{ Auth::user()->id }}" required readonly>
			</div>

			<div class="form-group">
				<label>{{ $clubs->name }}</label>
				<img src='{{ asset("storage/$clubs->image_location") }}' class="h-40" alt="...">
				<input type="hidden" class="form-control" name="club_id" value="{{ $clubs->id }}" required readonly>
			</div>

			<button type="submit" class="btn btn-success btn-block">Proceed</button>
	</form>
	
@endsection

@section('content')

	<div class="container-fluid">
		<div class="row">
			<div class="col-6 mx-auto">
				
				<h3 class="text-center">Rent Item</h3>

				<div class="card">
					
					<div class="card-header">Overview</div>

					<div class="card-body">
						
						@yield('rent-item-form')

					</div>
				</div>
			</div>
		</div>
	</div>

@endsection