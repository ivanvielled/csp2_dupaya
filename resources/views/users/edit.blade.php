@extends('layouts.app')

@section('title', 'Edit User')

@section('edit-user-form')
	
	<form action='{{ url("/users/$users->id")}}' method="post" enctype="multipart/form-data">
		@csrf

		@method("PUT")

			<div class="form-group">
				<label>Name</label>
				<input type="text" class="form-control" value="{{ $users->name }}" name="name">
			</div>

			<div class="form-group">
				<label>Username</label>
				<input type="text" class="form-control" value="{{ $users->username }}" name="username">
			</div>

			<div class="form-group">
				<label>Email</label>
				<input type="text" class="form-control" value="{{ $users->email }}" name="email">
			</div>

			<input type="hidden" class="form-control" value="{{ $users->user_role }}" name="user_role">
			<input type="hidden" class="form-control" value="{{ $users->password }}" name="password">

			<button type="submit" class="btn btn-success btn-block">Update</button>
	</form>
@endsection

@section('content')
	<div class="container-fluid">
		
		<div class="row">
			
			<div class="col-6 mx-auto">
				
				<h3 class="text-center">Edit User</h3>

				<div class="card">
					
					<div class="card-header">Item Information</div>

					<div class="card-body">
						
						@yield('edit-user-form')

					</div>
				</div>
			</div>
		</div>
	</div>
@endsection