@extends('layouts.app')

@section('title', 'Transaction Page')

@section('transactions')

	@foreach ($transactions as $transaction)
	
		@if (!empty(Auth::user()))

				<div class="card mb-2">
					
					<div class="card-header">
						
						<a class="card-link" data-toggle="collapse" href='#collapse-{{ $transaction["id"] }}'>{{ date_format(date_create($transaction->created_at), "M d, Y - h:i:s A") }}</a>
					</div>

					<div id='collapse-{{ $transaction["id"] }}' class="collapse" data-parent="#accordion">
						
						<div class="card-body">
							
							<table class="table table-bordered">
								
								<thead>
									
									<tr>
										<th>User</th>
										<th>Item</th>
										<th>Status</th>
									</tr>
								</thead>

								<tbody>
									<tr>
										<td>{{ $transaction->user->username }}</td>
										<td>{{ $transaction->rent->name }}</td>
										<td>{{ $transaction->status->name }}</td>
									</tr>
								</tbody>
							</table>
						@if (Auth::user()->user_role == 'admin')
							@if ($transaction->status_id == 4)
								<div class="btn-group btn-block">
									<form action='{{ url("/transactions/$transaction->id") }}' method="post">
										@csrf
										@method("PUT")
											<button type="submit" class="btn btn-success">Accept</button>
									</form>

									<form action="" method="post">
										@csrf
										@method("PUT")
											<button type="submit" class="btn btn-danger">Deny</button>
									</form>
								</div>
							@else
								<div class="btn-group btn-blocl">
									<form action="" method="post">
										@csrf
										@method("PUT")

											<button type="submit" class="btn btn-primary">Return</button>
									</form>
								</div>
							@endif
						@else
							@if (Auth::user()->has_rented == 1)
								<h3 class="text-center">Your golf club is ready for pickup.</h3>
							@endif
						@endif					
						</div>
					</div>
				</div>
		@endif
	@endforeach

@endsection

@section('content')
	
	<div class="container-fluid">
		<h3>Transactions</h3>

		<div id="accordion">
			@yield('transactions')
		</div>
	</div>
@endsection