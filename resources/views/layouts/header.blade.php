<nav class="navbar navbar-expand-lg navbar-dark bg-dark">

	<a href="{{ url('/')}}" class="navbar-brand">{{ config('app.name', 'Don D') }}</a>

	<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="navbar">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse text-right" id="navbar">
		
		<ul class="navbar-nav mr-auto">
			
			<li class="nav-item">
				<a class="nav-link" href="{{ url('/menu') }}">GolfClubs</a>
			</li>

			@if (!empty(Auth::user()) && Auth::user()->user_role == 'admin')
				<li class="nav-item">
					<a class="nav-link" href="{{ url('/users') }}">View Users</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="{{ url('/register') }}">Register</a>
				</li>

			@endif

		</ul>

		<ul class="navbar-nav ml-auto">
			
			@guest

				<li class="nav-item">
					<a class="nav-link" href="{{ url('/login') }}">Login</a>
				</li>

			@else 			

				<li class="nav-item">
					<a class="nav-link" href="{{ url('/transactions') }}">Transactions</a>
				</li>

				<li class="nav-item">
					<a onclick="document.querySelector('#logout-form').submit()" class="nav-link" href="#">Logout</a>
				</li>

			@endguest

		</ul>

	</div>
	
</nav>

<form id="logout-form" class="d-none" action="{{ route('logout') }}" method="POST">
	@csrf
</form>