@extends('layouts.app')

@section('title', 'Transaction Page')

@section('transactions')

	@foreach ($transactions as $transaction)
	
		@if (!empty(Auth::user()))
			

				<div class="card mb-2">
					
					<div class="card-header">
						
						<a class="card-link" data-toggle="collapse" href='#collapse-{{ $transaction["id"] }}'>{{ date_format(date_create($transaction->created_at), "M d, Y - h:i:s A") }}</a>
					</div>

					<div id='collapse-{{ $transaction["id"] }}' class="collapse" data-parent="#accordion">
						
						<div class="card-body">
							
							<table class="table table-bordered">
								
								<thead>
									
									<tr>
										<th>User</th>
										<th>Item</th>
										<th>Status</th>
									</tr>
								</thead>

								<tbody>
									<tr>
										<td>{{ $transaction->user->username }}</td>
										<td>{{ $transaction->rent->name }}</td>
										<td>{{ $transaction->status->name }}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			
		@endif
	@endforeach

@endsection

@section('content')
	
	<div class="container-fluid">
		<h3>Transactions</h3>

		<div id="accordion">
			@yield('transactions')
		</div>
	</div>
@endsection