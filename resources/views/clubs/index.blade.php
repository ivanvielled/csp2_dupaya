@extends('layouts.app')

@section('title', 'Menu')

@section('content')



		<div class="container-fluid">
			<h3>Welcome {{ Auth::user()->username }}</h3>
			<h3>Menu</h3>

			@if (!empty(Auth::user()) && Auth::user()->user_role == 'admin')
				<a href="{{ url('clubs/create') }}" class="btn btn-primary">Add Item</a>
			@endif

			<div class="row">
				
				@foreach ($clubs as $club)

					<div class="col-3 mt-3">

						<div class="card h-100">
							<img src='{{ asset("storage/$club->image_location") }}' class="card-img-top" width="100%" height="200px">

							<div class="card-body">
								
								<h4 class="card-title">{{ $club->name}} </h4>

								<p class="card-text">{{ $club->description }}</p>
								<p class="card-text">{{ $club->category->name }}</p>
								<p class="card-text">{{ $club->brand->name }}</p>
								<p class="card-text">{{ $club->color->name }}</p>
								<p class="card-text">{{ $club->availability->name }}</p>

								@if (!empty(Auth::user()))

									@if (Auth::user()->user_role == "admin")

										<div class="btn-group btn-block">
											<a class="btn btn-outline-info" href='{{ url("clubs/$club->id/edit") }}'>Edit</a>
											<a class="btn btn-outline-danger" href='{{ url("clubs/$club->id/delete-confirm") }}'>Delete</a>
										</div>

									@elseif (Auth::user()->user_role == "customer")

										<form class="">
											<div class="btn-group btn-block">
												<a href='{{ url("clubs/$club->id/rent")}}' class="btn btn-success">Rent</a>
											</div>
										</form>
									@endif
								@endif
							</div>
						</div>
					</div>
				@endforeach
			</div>

		</div>
@endsection

@if (!empty(session()->get('message')))
	<script>alert('{{ session()->get("message") }}'</script>
@endif