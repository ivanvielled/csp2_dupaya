@if (Auth::user()->user_role != 'admin')
	<script>window.location = '/menu'</script>
@endif

@extends('layouts.app')

@section('title', 'Add Item')

@section('add-item-form')
	
	<form action="{{ url('clubs/store') }}" method="post" enctype="multipart/form-data">

		@csrf
		
		<div class="form-group">
			<label>Item Name</label>
			<input type="text" class="form-control" name="name" required>
		</div>

		<div class="form-group">
			<label>Description</label>
			<input type="text" class="form-control" name="description">
		</div>

		<div class="form-group">
			<label>Category</label>
			<select class="form-control" name="category_id">
				<option value selected disabled>Select Category</option>
				@foreach ($categories as $category)
					<option value="{{ $category->id }}">{{ $category->name }}</option>
				@endforeach
			</select>
		</div>

		<div class="form-group">
			<label>Brand</label>
			<select class="form-control" name="brand_id">
				<option value selected disabled>Select Brand</option>
				@foreach ($brands as $brand)
					<option value="{{ $brand->id }}">{{ $brand->name }}</option>
				@endforeach
			</select>
		</div>

		<div class="form-group">
			<label>Color</label>
			<select class="form-control" name="color_id">
				<option value selected disabled>Select Color</option>
				@foreach ($colors as $color)
					<option value="{{ $color->id }}">{{ $color->name }}</option>
				@endforeach
			</select>
		</div>

		<div class="form-group">
			<label>Availability</label>
			<select class="form-control" name="availability_id">
				<option value selected disabled>Select Availability</option>
				@foreach ($availabilities as $availability)
					<option value="{{ $availability->id }}">{{ $availability->name }}</option>
				@endforeach
			</select>
		</div>

		<div class="form-group">
			<label>Image</label>
			<input type="file" class="form-control" name="image" required>
		</div>

		<button type="submit" class="btn btn-success btn-block">Add</button>

	</form>

@endsection

@section('content')
	
	<div class="container-fluid">
		<div class="row">
			<div class="col-6 mx-auto">
				
				<h3 class="text-center">Add Item</h3>

				<div class="card">
					
					<div class="card-header">Item Information</div>

					<div class="card-body">
						
						@yield('add-item-form')

					</div>
				</div>
			</div>
		</div>
	</div>
@endsection