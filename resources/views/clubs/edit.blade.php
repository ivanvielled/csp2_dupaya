@if (Auth::user()->user_role != 'admin')
	<script>window.location = '/menu'</script>
@endif

@extends('layouts.app')

@section('title', 'Edit Item')

@section('edit-item-form')
	
	<form action='{{ url("clubs/$clubs->id") }}' method="post" enctype="multipart/form-data">
		
		@csrf

		@method("PUT")

			<div class="form-group">
				<label>Item Name</label>
				<input type="text" class="form-control" name="name" value="{{ $clubs->name }}" required>
			</div>

			<div class="form-group">
				<label>Description</label>
				<input type="text" class="form-control" name="description" value="{{ $clubs->description }}">
			</div>

			<div class="form-group">
				<label>Category</label>
				<select class="form-control" name="category_id">
					<option value selected disabled>Select Category</option>
					@foreach ($categories as $category)
						@if ($category->id == $clubs->category_id)
							<option value="{{ $category->id }}" selected>{{ $category->name }}</option>
						@else
							<option value="{{ $category->id }}">{{ $category->name }}</option>
						@endif
					@endforeach
				</select>
			</div>

			<div class="form-group">
				<label>Brand</label>
				<select class="form-control" name="brand_id">
					<option value selected disabled>Select Brand</option>
					@foreach ($brands as $brand)

						@if ($brand->id == $clubs->brand_id)
							<option value="{{ $brand->id }}" selected>{{ $brand->name }}</option>
						@else
							<option value="{{ $brand->id}}">{{ $brand->name }}</option>
						@endif
					@endforeach
				</select>
			</div>

			<div class="form-group">
				<label>Color</label>
				<select class="form-control" name="color_id">
					<option value selected disabled>Select Color</option>
					@foreach ($colors as $color)

						@if ($color->id == $clubs->color_id)
							<option value="{{ $color->id }}" selected>{{ $color->name }}</option>
						@else
							<option value="{{ $color->id }}">{{ $color->name }}</option>
						@endif
					@endforeach
				</select>
			</div>

			<div class="form-group">
				<label>Availability</label>
				<select class="form-control" name="availability_id">
					<option value selected disabled>Select Availability</option>
					@foreach ($availabilities as $availability)

						@if ($availability->id == $clubs->availability_id)
							<option value="{{ $availability->id }}" selected>{{ $availability->name }}</option>
						@else
							<option value="{{ $availability->id }}">{{ $availability->name }}</option>
						@endif
					@endforeach
				</select>
			</div>

			<div class="form-group">
				<label>Image</label>
				<input type="file" class="form-control" name="image" required>
			</div>

			<button type="submit" class="btn btn-success btn-block">Update</button>

	</form>

@endsection

@section('content')

	<div class="container-fluid">
		
		<div class="row">
			
			<div class="col-6 mx-auto">
				
				<h3 class="text-center">Edit Item</h3>

				<div class="card">
					
					<div class="card-header">Item Information</div>

					<div class="card-body">
						
						@yield('edit-item-form')

					</div>
				</div>
			</div>
		</div>
	</div>

@endsection