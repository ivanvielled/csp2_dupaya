<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/menu', 'ClubController@index');

Route::get('/clubs/create', 'ClubController@create');
Route::post('/clubs/store', 'ClubController@store');

Route::get('/clubs/{id}/edit', 'ClubController@edit');
Route::put('/clubs/{id}', 'ClubController@update');

Route::get('/clubs/{id}/delete-confirm', 'ClubController@deleteConfirm');
Route::delete('/clubs/{id}', 'ClubController@destroy');

Route::get('users', 'UserController@index');
Route::get('/users/{id}/edit', 'UserController@edit');
Route::put('/users/{id}', 'UserController@update');
Route::get('/users/{id}/delete-confirm', 'UserController@deleteConfirm');
Route::delete('/users/{id}', 'UserController@destroy');

Route::get('clubs/{id}/rent', 'TransactionController@rent');
Route::post('/rent/store', 'TransactionController@store');

Route::get('transactions', 'TransactionController@index');
Route::put('/transactions/{id}', 'TransactionController@accept');


