<?php

use Illuminate\Database\Seeder;
use App\Brand;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Brand::create(['name' => 'Callaway']);
        Brand::create(['name' => 'TaylorMade']);
        Brand::create(['name' => 'Tour Edge']);
    }
}
