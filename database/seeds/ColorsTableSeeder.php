<?php

use Illuminate\Database\Seeder;
use App\Color;

class ColorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Color::create(['name' => 'Jet Black']);
        Color::create(['name' => 'Tour Chrome']);
        Color::create(['name' => 'Brushed Steel']);
    }
}
