<?php

use Illuminate\Database\Seeder;
use App\Availability;

class AvailabilitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Availability::create(['name' => 'Yes']);
        Availability::create(['name' => 'No']);
    }
}
